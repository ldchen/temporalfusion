# Temporal Fusion: Continuous-Time Light Field Video Factorization


Project Link: [Temporal Fusion](https://www.ee.nthu.edu.tw/vcslab/lfd_tf)


## Run
Run factorization
``` shell
python src/Video_NTF.py --help
```

## Citation
tbd

## Acknowledgement
This work was supported partly by Raydium Semiconductor Corporation.