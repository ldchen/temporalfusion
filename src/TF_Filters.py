# %%
from datetime import time
import numpy as np
import matplotlib.pyplot as plt

# SAMPLE = 10000
TICK_PRECISION = 0.0005
#%%
def integral(x: np.ndarray, dt=TICK_PRECISION) -> float:
    """perform integral on x(t)

    Args:
        x (np.ndarray): array of x(t), with t in integral range of

    Returns:
        float: integral result
    """
    return x.sum() * dt


def uniform(t, Tf):
    """uniform fusion

    Args:
        t (np.ndarray): evaluated time stamps
        Tf (float): fusion window length

    Returns:
        np.ndarray: g(t)
    """
    return rect(t / Tf - 0.5) / Tf


def inverse_exp(t, Tf):
    if 0 <= t <= Tf:
        y = 1 - np.exp(-(Tf - t))
    else:
        y = 0
    return y


def inverse_linear(t, Tf):
    if 0 <= t <= Tf:
        y = Tf - t
    else:
        y = 0
    return y


def rect(t):
    """rectangular function

    Args:
        t (np.ndarray): evaluated point

    Returns:
        np.ndarray: filter result
    """
    result = np.zeros(t.shape)
    k = np.logical_and(t < 0.5, t >= -0.5)
    result[k] = 1
    return result


def convolve(a, b, dt):
    """perform convolution for two filters
    Assumes that both filter has same time samples and equal length
    Args:
        a (np.ndarray): filter targer A
        b (np.ndarray): filter target B
        norm (bool, optional): normalize with max value Defaults to True.
    """
    seg = a.shape[0] // 2
    c = np.convolve(a, b) * dt  # linear convolution
    c = c[seg : -(seg) + 1]
    return c


class TF_Filter:
    def __init__(self, f, time_idx, length=0):
        if callable(f):
            self.ft = f(time_idx)
        else:
            self.ft = f
        self.time = time_idx
        self.end = length


def ceil(a):
    return int(-(-a // 1))


def floor(a):
    return int(a // 1)


def objective_filter(Tv: int, gwt: TF_Filter, t_range: tuple):
    """genetate filter U[k] from the fusion filter g(t)

    Args:
        Tv (int): Target video refresh interval
        Ts (int): factorized video refresh interval
        gwt (ContFilter): fusion filter
    """
    # t_start should be 0, otherwise it generates 0 weight points
    t_start, t_end = t_range
    t = gwt.time  # continuous time index
    start = floor(t_start / Tv)
    end = ceil(t_end / Tv)
    # generate discrete index omega_k
    omega_k = list(range(start, end))
    u = np.zeros_like(omega_k, dtype=float)
    for k_i, k in enumerate(omega_k):
        # Discretize: u[k_i] = int gw(t) for t in [kTv, (k+1)Tv)
        v_start, v_end = k * Tv, (k + 1) * Tv
        integral_range = np.logical_and(t >= v_start, t < v_end)
        u[k_i] = integral(gwt.ft[integral_range])
    return TF_Filter(u, omega_k)


def factorized_filter(Ts: int, gwt: TF_Filter, dt, t_range):
    """genetate filter V[k] from the fusion filter g(t)

    Args:
        Ts (int): factorized video refresh interval
        gwt (ContFilter): fusion filter
    """
    Ts_point = int(Ts / dt)
    t_start, t_end = t_range
    start = floor(t_start / Ts) + 1
    end = ceil(t_end / Ts)
    omega_k = list(range(start, end))
    v = np.zeros_like(omega_k, dtype=float)
    t = gwt.time
    for ki, k in enumerate(omega_k):
        # Discretize: v[k_i] = int gw(t)gw_k(t) for t in [kTs, (k+1)Ts)
        gwkt = np.roll(gwt.ft, Ts_point * k)
        x_min = max(0, k * Ts, start)
        x_max = min(gwt.end, k * Ts + gwt.end, end)
        # v[ki] = \int gw_k(t)* gw(t)
        gg = gwkt * gwt.ft
        integral_range = np.logical_and(t >= x_min, t < x_max)
        v[ki] = 0 if len(integral_range) == 0 else integral(gg[integral_range])
    return TF_Filter(v, omega_k)


def fusion_filter(_f, Tf):
    def f(t):
        return _f(t, Tf)

    return f


def generate_tf_filter(Tf, Tv, Ts):
    """Temporal fusion filter

    Args:
        Tf (float): fusion window
        Tv (float): refresh period of target light field
        Ts (float): refresh period of factorized frames

    Returns:
        tuple(TF_Filter, TF_Filter) : u, v
    """
    nT = Tf + Ts
    time_range = (-2 * (nT), 2 * (nT))
    samples = (time_range[1] - time_range[0]) * int(1 / TICK_PRECISION)
    t = np.linspace(-2 * (nT), 2 * (nT), samples)
    # Setup g(t)
    t = np.linspace(*time_range, samples)
    gt = TF_Filter(fusion_filter(uniform, Tf), t, Tf)
    tr = rect(t / Ts - 0.5)
    gwt = convolve(gt.ft, tr, TICK_PRECISION)
    gwt = TF_Filter(gwt, t, gt.end + Ts)
    # Two side
    a = objective_filter(Tv, gwt, (0, gwt.end))
    b = factorized_filter(Ts, gwt, TICK_PRECISION, (-gwt.end, gwt.end))
    return a, b


def generate_tf_back_filter(Tf, Tv, Ts):
    """Backward only temporal fusion filter

    Args:
        Tf (float): fusion window
        Tv (float): refresh period of target light field
        Ts (float): refresh period of factorized frames

    Returns:
        tuple(TF_Filter, TF_Filter) : u, v
    """
    nT = Tf + Ts
    time_range = (-2 * (nT), 2 * (nT))
    samples = (time_range[1] - time_range[0]) * int(1 / TICK_PRECISION)
    t = np.linspace(-2 * (nT), 2 * (nT), samples)
    # Setup g(t)
    gt = TF_Filter(fusion_filter(uniform, Tf), t, Tf)
    tr = rect(t / Ts - 0.5)
    gwt = convolve(gt.ft, tr, TICK_PRECISION)
    gwt = TF_Filter(gwt, t, gt.end + Ts)
    # Single side
    u = objective_filter(Tv, gwt, (0, Ts))
    v = factorized_filter(Ts, gwt, TICK_PRECISION, (-gwt.end, Ts))
    return u, v


def genObjFilter(Tf: float, Te: float, Tv=1, flip=True, Threshold=5e-3, causal=False):
    Th = Tf + Te
    TH = int(Th + 0.5)
    time_range = (-2 * (TH), 2 * (TH))
    samples = (time_range[1] - time_range[0]) * int(1 / TICK_PRECISION)
    t = np.linspace(-2 * (TH), 2 * (TH), samples)
    # Assume uniform here
    g_t = uniform(t, Tf)  # fusion_filter(uniform, Th)(t)
    r_t = rect(t / Te - 0.5)
    h_t = convolve(g_t, r_t, TICK_PRECISION)
    # b[n] = h integraled as discrete segment
    if causal:
        n_range = (0, 1)
    else:
        # b[n] = \int_{-nT_E}^{(-n+1)T_E} h(t) dt
        n_range = (-TH - 1, 1)
    b_idx = list(np.arange(*n_range))
    b = []
    for n in b_idx:
        integral_range = np.logical_and(t >= -n * Tv, t < (-n + 1) * Tv)
        value = integral(h_t[integral_range])
        b.append(value)
    # return (b_idx, b)
    if flip:
        b_idx = [-v for v in b_idx[::-1]]
        b = b[::-1]
    # filter small weight under Threshold
    r_idx = [i for i, v in zip(b_idx, b) if v >= Threshold]
    r = [v for v in b if v >= Threshold]
    return (r_idx, r)


def genFacFilter(Tf: float, Te: float, flip=True, Threshold=5e-3, causal=False):
    Th = Tf + Te
    TH = int(Th + 0.5)
    time_range = (-2 * (TH), 2 * (TH))
    samples = (time_range[1] - time_range[0]) * int(1 / TICK_PRECISION)
    t = np.linspace(-2 * (TH), 2 * (TH), samples)
    # Assume uniform here
    g_t = uniform(t, Tf)  # fusion_filter(uniform, Tf)(t)
    r_t = rect(t / Te - 0.5)
    h_t = convolve(g_t, r_t, TICK_PRECISION)
    # a[n] = h(t) \cdot h(t+nT_E) integraled as discrete segment
    # a[n] = \int_{0}^{Th} h(t) \cdot h(t+nT_E) dt
    if causal:
        integral_range = np.logical_and(t >= 0, t < Te)
    else:
        integral_range = np.logical_and(t >= 0, t < Th)
    a = []
    a_idx = list(np.arange(-TH, TH + 1))
    for n in a_idx:
        hn_t = np.roll(h_t, -int(n * Te / TICK_PRECISION))
        h_auto = hn_t * h_t
        value = integral(h_auto[integral_range])
        a.append(value)
    # return (a_idx, a)
    if flip:
        a_idx = [-v for v in a_idx[::-1]]
        a = a[::-1]
    # filter small weight under Threshold
    r_idx = [i for i, v in zip(a_idx, a) if v >= Threshold]
    r = [v for v in a if v >= Threshold]
    return (r_idx, r)


def tf_flow():
    bi, b = genObjFilter(4, 1, flip=False)
    plt.stem(bi, b)
    plt.show()
    print(bi)
    print(b)
    ai, a = genFacFilter(4, 1, flip=False)
    plt.stem(ai, a)
    plt.show()
    print(ai)
    print(a)
    print(sum(a), sum(b))
    bi, b = genObjFilter(4, 1, flip=False)
    plt.stem(bi, b)
    plt.show()
    print(bi)
    print(b)
    ai, a = genFacFilter(4, 1, flip=False)
    plt.stem(ai, a)
    plt.show()
    print(ai)
    print(a)
    print(sum(a), sum(b))


#%%
if __name__ == "__main__":
    Tf, Te, Tv = 4, 1, 1
    print(f"Using parameters (Tf, Te, Tv) = {Tf, Te, Tv}")
    print("Generate a[n] and b[n] non-causal")
    ai, a = genFacFilter(Tf, Te, flip=False)
    bi, b = genObjFilter(Tf, Te, flip=False)
    print(f"  n : ", end="")
    print([f"{i:>4}" for i in ai])
    print(f"a[n]: ", end="")
    print([f"{i:.2f}" for i in a])
    print(f"  n : ", end="")
    print([f"{i:>4}" for i in bi])
    print(f"b[n]: ", end="")
    print([f"{i:.2f}" for i in b])
    print("Generate a[n] and b[n] causal")
    ai, a = genFacFilter(Tf, Te, causal=True, flip=False)
    bi, b = genObjFilter(Tf, Te, causal=True, flip=False)
    print(f"  n : ", end="")
    print([f"{i:>4}" for i in ai])
    print(f"a[n]: ", end="")
    print([f"{i:.2f}" for i in a])
    print(f"  n : ", end="")
    print([f"{i:>4}" for i in bi])
    print(f"b[n]: ", end="")
    print([f"{i:.2f}" for i in b])
# %%
