# %%
import numpy as np
import cv2 as cv
from lff.DvStruct import DvStruct
from pathlib import Path
from joblib import Parallel, delayed
from distutils.dir_util import copy_tree
from tqdm import tqdm
import threading
import argparse
from itertools import product as iterProduct


# import json
import argparse
from functools import reduce
from tqdm import tqdm
import cupy as cp
from copy import deepcopy

import time
from string import Template

# %%
from lff.config import DEBUG, INFO, dtype, read_config
from lff.lf_fact import image_mode_lf_factor_given
from lff.lf_fact import NTFStruct, generate_blks
from lff.lf_fact import lf_synthesis_g, lf_fact_gg, lf_fact_weight_gg, setup_mv, psnr
from lff.TF_Filters import genFacFilter, genObjFilter
from skimage.metrics import structural_similarity


# %%
def load_mv(mv_folder: Path, v_shape: tuple[int, int]):
    images = Parallel(n_jobs=16)(
        delayed(cv.imread)(*args)
        for args in [
            (str(mv_folder / f"cam_{_vv:02}_{_hv:02}.png"), cv.IMREAD_UNCHANGED)
            for _vv, _hv in np.ndindex(v_shape)
        ]
    )
    _shape = images[0].shape
    return (np.concatenate(images) / 255.0).astype(dtype).reshape(v_shape + _shape)


def one_frame_ntf(input, out, dv_s, ntf_s):
    image_mode_lf_factor_given(input, out, dv_s, ntf_s, synthesis=False)


def direct_ntf_frame_mapping(frames: list[int], rank: int):
    """
    generate mapped frames for direct ntf
    Args:
        frames (list[int]): input frames
        rank (int): rank in direct ntf
    Returns:
        list[int]: mapped frames
    """
    sub_frames = [_ - rank + 1 for _ in range(rank)]
    frames = [[f + _ for _ in sub_frames] for f in frames]
    return reduce(lambda a, b: a + b, frames)


def chunk(L, step) -> list[int]:
    for i in range(0, len(L), step):
        yield L[i : i + step]


class MV_VideoField:
    blkT = "blk_{an_y}_{an_x}_{w_y}_{w_x}"
    viewT = "cam_{vv}_{hv}.png"
    frameT = "{:06}"

    def __init__(self, folder: Path, shape: tuple, chNum: int) -> None:
        self.folder = folder
        self.shape = shape
        self.chNum = chNum
        pass

    def getBlk(
        self,
        f: int,
        blk,
    ):
        frameFolder = self.folder / self.frameT.format(f)
        y, x = blk[2] + dv_s.offset[0] * 2, blk[3] + dv_s.offset[1] * 2
        mode = cv.IMREAD_UNCHANGED if self.chNum == 1 else cv.IMREAD_COLOR
        blkfolder = frameFolder / self.blkT.format(
            an_y=blk[0], an_x=blk[1], w_y=y, w_x=x
        )
        mvs = [
            cv.imread(
                str(blkfolder / self.viewT.format(vv=vv, hv=hv)),
                mode,
            )
            for vv, hv in np.ndindex(self.vv, self.hv)
        ]
        shape = (
            self.shape[:2] + [y, x]
            if self.chNum == 1
            else self.shape + [y, x] + (self.chNum,)
        )
        mv = np.stack(mvs).reshape(*shape)
        return mv

    def get(self, f: int, slice=None):
        frameFolder = self.folder / self.frameT.format(f)
        mode = cv.IMREAD_UNCHANGED if self.chNum == 1 else cv.IMREAD_COLOR
        mvs = [
            cv.imread(str(frameFolder / self.viewT.format(vv=vv, hv=hv)), mode)
            for vv, hv in np.ndindex(self.vv, self.hv)
        ]
        shape = self.shape if self.chNum == 1 else self.shape + (self.chNum,)
        mv = np.stack(mvs).reshape(*shape)
        if slice is None:
            return mv
        else:
            return mv[slice]
        # if mvs[0].shape[0] != 256:
        #     new_x = int((256/mvs[0].shape[0])*mvs[0].shape[1])
        #     tmp = [cv.resize(_i, (new_x, 256)) for _i in mvs]
        #     center = new_x//2
        #     tmp = [_i[:, center-128:center+128] for _i in tmp]
        #     tmp = np.stack(tmp)
        #     return tmp.reshape(*shape)
        return

    def save(self, f: int, buffer: np.ndarray):
        frameFolder = self.folder / self.frameT.format(f)
        frameFolder.mkdir(parents=True, exist_ok=True)
        [
            cv.imwrite(
                str(frameFolder / self.viewT.format(vv=vv, hv=hv)), buffer[vv, hv]
            )
            for vv, hv in np.ndindex(self.vv, self.hv)
        ]

    def saveBlk(self, f: int, buffer: np.ndarray, blks):
        frameFolder = self.folder / self.frameT.format(f)
        for blk in blks:
            y, x = blk[2] + dv_s.offset[0] * 2, blk[3] + dv_s.offset[1] * 2
            blk_folder = frameFolder / self.blkT.format(
                an_y=blk[0], an_x=blk[1], w_y=y, w_x=x
            )
            blk_folder.mkdir(parents=True, exist_ok=True)
            x_region = slice(blk[0], blk[0] + blk[2] + dv_s.offset[0] * 2), slice(
                blk[1], blk[1] + blk[3] + dv_s.offset[1] * 2
            )
            mvSlice = (slice(None),) * 2 + x_region
            curMv = buffer[mvSlice]
            [
                cv.imwrite(
                    str(blk_folder / self.viewT.format(vv=vv, hv=hv)),
                    curMv[vv, hv],
                )
                for vv, hv in np.ndindex(self.vv, self.hv)
            ]

    @property
    def vv(self):
        return self.shape[0]

    @property
    def hv(self):
        return self.shape[1]

    @property
    def vp(self):
        return self.shape[2]

    @property
    def hp(self):
        return self.shape[3]


class DV_VideoField:
    layerT = "dv_{l}_{f:06}.png"

    def __init__(self, folder: Path, dv_s: DvStruct) -> None:
        """
        Initializes a new instance of Display Video Field.

        :param folder: A Path object representing the folder path.
        :type folder: Path
        :param dv_s: A DvStruct object representing the DV structure.
        :type dv_s: DvStruct
        :return: None.
        :rtype: None
        """
        self.folder = folder
        self.dv_s = dv_s
        pass

    def save(self, n_: list[int], buffer: np.ndarray, scale=255):
        """
        Saves the given buffer to disk as images.

        :param n_: A list of integers representing the node indices.
        :type n_: list[int]
        :param buffer: A numpy ndarray representing the image to be saved.
        :type buffer: np.ndarray
        :param scale: An optional scaling factor for the image. Defaults to 255.
        :type scale: int
        """
        buffer_out = (buffer * scale).astype(np.uint8)
        self.folder.mkdir(parents=True, exist_ok=True)
        [
            cv.imwrite(
                str(self.folder / self.layerT.format(l=l[1], f=n)), buffer_out[r, l[0]]
            )
            for l in self.dv_s.layers
            for r, n in enumerate(n_)
        ]

    def get(self, n_: list[int]):
        def _read(path):
            """
            Reads an image from the specified path.

            Args:
                path (str): A string that represents the path to the image.

            Returns:
                numpy.ndarray: An array that represents the image.
            """
            DEBUG(path)
            return cv.imread(path, cv.IMREAD_UNCHANGED)

        dvs = [
            _read(str(self.folder / self.layerT.format(f=n, l=l[1])))
            for r, n in enumerate(n_)
            for l in self.dv_s.layers
        ]
        return np.stack(dvs).reshape(len(n_), *self.dv_s.dv_shape[1:])


def new_dv(frames: int, dv_s: DvStruct, dtype=np.float16, rand=True) -> np.ndarray:
    # prepare dv
    if rand:
        dv = np.random.rand(frames, *dv_s.dv_shape[1:]).astype(dtype) + 0.2
        dv = dv.clip(0, 1)
    else:
        dv = np.ones((frames,) + dv_s.dv_shape[1:], dtype=dtype)
    mask_dv(dv, dv_s)
    return dv


def mask_dv(dv, dv_s: DvStruct):
    of_vert, of_hori = dv_s.offset
    vx, hx = dv_s.x_shape
    vs, hs = dv_s.step
    # Mask boundaries
    dv[:, :, :of_vert, :] = 0
    dv[:, :, :, :of_hori] = 0
    for l in dv_s.layers:
        l_idx, l_pos = l
        # print(l, hs, vx+of_vert+l*vs)
        dv[:, l_idx, vx + of_vert + l_pos * vs :, :] = 0
        dv[:, l_idx, :, hx + of_hori + l_pos * hs :] = 0


def vntf_ff(
    dv_s: DvStruct,
    ntf_s: NTFStruct,
    mvvf: MV_VideoField,
    dvvf: DV_VideoField,
    cached=False,
):
    # Frame-wise Factorization (FF)
    dv_s = deepcopy(dv_s)
    rank = dv_s.rank
    m_sampled = mvvf.frames[rank - 1 :: rank]
    n_sampled = [
        list(range(n - rank + 1, n + 1)) for n in dvvf.frames[rank - 1 :: rank]
    ]
    dv_s.set_shape(*mvvf.shape)
    dvvf.dv_s = dv_s
    for m, n_ in tqdm(list(zip(m_sampled, n_sampled))):
        mvFrame = (mvvf.get(m) / 255.0).astype(dtype)
        mvpad = ((0, 0),) * 2 + ((dv_s.offset[0],) * 2,) + ((dv_s.offset[1],) * 2,)
        mvFrame = np.pad(mvFrame, mvpad)
        if cached:
            dv = (dvvf.get(n_) / 255.0).astype(dtype)
        else:
            dv = new_dv(rank, dv_s)
        one_frame_ntf(mvFrame * rank, dv_s=dv_s, out=dv, ntf_s=ntf_s)
        dvvf.save(n_, dv)


def vntf_rrff(
    dv_s: DvStruct,
    ntf_s: NTFStruct,
    mvvf: MV_VideoField,
    dvvf: DV_VideoField,
    fFilter: np.ndarray,
    cached=False,
):
    # Residue Removal Frame-wise Factorization (BRR)
    dv_s = deepcopy(dv_s)
    rank = dv_s.rank
    m_sampled = mvvf.frames[rank - 1 :: rank]
    n_sampled = [
        list(range(n - rank + 1, n + 1)) for n in dvvf.frames[rank - 1 :: rank]
    ]
    dv_s.set_shape(*mvvf.shape)
    mvpad = ((0, 0),) * 2 + ((dv_s.offset[0],) * 2,) + ((dv_s.offset[1],) * 2,)
    sampled_frames = list(zip(m_sampled, n_sampled))
    ## Head frame (factorized by FF)
    m, n_ = sampled_frames[0]
    mvFrame = (mvvf.get(m) / 255.0).astype(np.float16)
    mvFrame = np.pad(mvFrame, mvpad)
    if cached:
        dv = (dvvf.get(n_) / 255.0).astype(dtype)
    else:
        dv = new_dv(rank, dv_s)
    one_frame_ntf(mvFrame * rank, dv_s=dv_s, out=dv, ntf_s=ntf_s)
    dvvf.save(n_, dv)
    ## Reset frames
    fFilter = cp.array(fFilter)
    f_residue = fFilter[: rank - 1]
    f_current = cp.expand_dims(fFilter[-rank:], axis=(1, 2, 3, 4))
    rdv_s = deepcopy(dv_s).set_rank(rank - 1)
    dvG = cp.array(dv)
    dv_prevG = dvG[1:].copy()
    mvFrameG = cp.zeros(dv_s.mv_shape, dtype=dtype)
    core = (
        slice(None),
        slice(None),
        slice(dv_s.offset[0], -dv_s.offset[0]),
        slice(dv_s.offset[1], -dv_s.offset[1]),
    )
    for m, n_ in tqdm(sampled_frames[1:]):
        mvFrameG[core] = cp.array((mvvf.get(m) / 255.0).astype(np.float16))
        residue = lf_synthesis_g(dv_prevG, rdv_s, f_residue)
        mvFrameG[core] -= residue
        mvFrameG.clip(0, 1, out=mvFrameG)
        lf_fact_weight_gg(mvFrameG, dvG, dv_s, f_current, ntf_s)
        dvvf.save(n_, dvG.get())
        dv_prevG[:] = dvG[1:]


def vntf_rrsf(
    dv_s: DvStruct,
    ntf_s: NTFStruct,
    mvvf: MV_VideoField,
    dvvf: DV_VideoField,
    fFilter: np.ndarray,
    cached=False,
    cont=False,
):
    # Residue Removal Sub-frame-wise Factorization (TFC)
    dv_s = deepcopy(dv_s)
    mvpad = ((0, 0),) * 2 + ((dv_s.offset[0],) * 2,) + ((dv_s.offset[1],) * 2,)
    rank = dv_s.rank
    if cont == False:
        mvvf.frames = [
            mvvf.frames[int((((n + 0.5) // rank) + 1) * rank - 1)]
            for n in range(len(mvvf.frames))
        ]
    m_sampled = mvvf.frames[rank - 1 : :]
    n_sampled = dvvf.frames[rank - 1 : :]
    ## Head frame (factorized by FF)
    dv_s.set_shape(*mvvf.shape)
    sampled_frames = list(zip(m_sampled, n_sampled))
    m, n_ = sampled_frames[0]
    dv = new_dv(rank, dv_s)
    mvFrame = (mvvf.get(m) / 255.0).astype(np.float16)
    mvFrame = np.pad(mvFrame, mvpad)
    one_frame_ntf(mvFrame * rank, dv_s=dv_s, out=dv, ntf_s=ntf_s)
    dvvf.save(list(range(n_ - rank + 1, n_ + 1)), dv)
    ## Rest frames
    fFilter = cp.array(fFilter)
    fFilter /= fFilter.sum()
    f_residue = fFilter[:-1]
    print("Residue = ")
    print(f_residue)
    dvG = cp.array(dv)
    dvs_one = deepcopy(dv_s).set_rank(1)
    dv_base = cp.ones(dvs_one.dv_shape, dtype=np.float16) * 0.5
    mask_dv(dv_base, dvs_one)
    mvFrameG = cp.zeros(dv_s.mv_shape, dtype=dtype)
    core = (
        slice(None),
        slice(None),
        slice(dv_s.offset[0], -dv_s.offset[0]),
        slice(dv_s.offset[1], -dv_s.offset[1]),
    )
    for m, n in tqdm(sampled_frames[1:]):
        ori = cp.array((mvvf.get(m) / 255.0).astype(np.float16))
        # mvFrameG[core] = ori*fFilter.sum()
        residue = lf_synthesis_g(dvG, dv_s, f_residue)
        mvFrameG[core] = ori - residue
        mvFrameG /= fFilter[-1]
        mvFrameG.clip(0, 1, out=mvFrameG)
        # cv.imwrite('a.png',((mvFrameG.get()[0,0]*255)).astype(np.uint8))
        dvg = dv_base.copy()
        lf_fact_gg(mvFrameG, dvg, dvs_one, ntf_s)
        dvG[0:-1] = dvG[1:]
        dvG[-1] = dvg
        dvvf.save([n], dvg.get())


def vntf_sf(
    dv_s: DvStruct,
    ntf_s: NTFStruct,
    mvvf: MV_VideoField,
    dvvf: DV_VideoField,
    fFilters: list[np.ndarray],
    cont=False,
):
    # Sub-frame-wise Factorization (TF)
    rank = dv_s.rank
    if cont == False:
        mvvf.frames = [
            mvvf.frames[int((((n + 0.5) // rank) + 1) * rank - 1)]
            for n in range(len(mvvf.frames))
        ]
    m_sampled = mvvf.frames[rank - 1 : :]
    n_sampled = dvvf.frames[rank - 1 : :]
    dv_s.set_shape(*mvvf.shape)
    sampled_frames = list(zip(m_sampled, n_sampled))

    # dvvf.save(n_, dv1)

    ## Rest frame
    filter_b = fFilters[0]  # objective
    filter_a = fFilters[1]  # factorize
    alen = len(filter_a)
    blen = len(filter_b)
    b = cp.expand_dims(cp.array(filter_b), axis=tuple(range(1, 4 + 1)))  # objective
    a = cp.expand_dims(cp.array(filter_a), axis=tuple(range(1, 4 + 1)))  # factorize

    r = dv_s.rank
    FullS = len(dvvf.frames)
    dvg = (
        cp.random.rand(*((FullS, len(dv_s.layers)) + dv_s.frame_shape)).astype(dtype)
        + 0.1
    )
    dvg[r:-r] = 0.5
    dv_s_full = deepcopy(dv_s)
    dv_s_full.set_rank(FullS)
    mask_dv(dvg, dv_s_full)

    ## HEAD/Tail frame (factorized by FF)
    mvpad = ((0, 0),) * 2 + ((dv_s.offset[0],) * 2,) + ((dv_s.offset[1],) * 2,)
    for dvDst, mn in zip([dvg[:r], dvg[-r:]], [sampled_frames[0], sampled_frames[-1]]):
        m, n = mn
        mvFrame = (mvvf.get(m) / 255.0).astype(dtype)
        mvFrame = np.pad(mvFrame, mvpad)
        one_frame_ntf(mvFrame * rank, dv_s=dv_s, out=dvDst, ntf_s=ntf_s)

    ## Pre-Filter
    S = len(sampled_frames)
    mv_seg = np.empty((S,) + dv_s.base_shape, dtype=dtype)
    core = (
        slice(None),
        slice(None),
        slice(None),
        slice(dv_s.offset[0], -dv_s.offset[0]),
        slice(dv_s.offset[1], -dv_s.offset[1]),
    )
    for idx, mn in enumerate(sampled_frames):
        m = mn[0]
        mv_seg[idx] = mvvf.get(m)
    mvg = cp.zeros((S,) + dv_s.v_shape + dv_s.frame_shape, dtype=dtype)
    mvg[core] = cp.array(mv_seg) / 255.0
    for m in range(0, S - r):
        mvg[m] = (mvg[m : m + blen] * b).sum(axis=0)
        # cv.imwrite(f'f{m}.png', (mvg[m, 0 ,0].get()*255).astype(np.uint8))
    dv_run = deepcopy(dv_s)
    dv_run.set_rank(2 * r + 1)
    ntf_run = NTFStruct(iteration=(1, 1), block=ntf_s.block)
    for iiter in tqdm(range(ntf_s.iteration[1])):
        for m, n in enumerate(n_sampled[1:-r]):
            lf_fact_weight_gg(
                mvg[m], dvg[n - r : n + r + 1], dv_run, a, ntf_run, uidx=slice(r, r + 1)
            )
    dvvf.save(list(range(S + r - 1)), dvg.get())


def app_direct_video_ntf(dv_s, ntf_s, video_config, chs=range(3)):
    for ch in chs:
        DEBUG(f"Processing ch {ch}")
        # single_direct_video_ntf(dv_s, ntf_s, video_config, ch)
        vntf_ff(dv_s, ntf_s, video_config, ch)


def color_split(
    mv_shape: tuple[int], src: Path, dst: Path, frames: list[int], offset=0
):
    srcMV = MV_VideoField(src, mv_shape, chNum=3)
    dstMVs = [MV_VideoField(dst / f"ch{ch}", mv_shape, chNum=1) for ch in range(3)]
    print(f"Color Split for {src}")
    for f in tqdm(frames):
        mvFrame = srcMV.get(f + offset)
        for ch, dMV in zip(range(3), dstMVs):
            dMV.save(f, mvFrame[..., ch])



def lf_synthesis(dv_s: DvStruct, dst: MV_VideoField, dvvf: DV_VideoField):
    # Synthesis
    dv_s = deepcopy(dv_s)
    rank = dv_s.rank
    m_sampled = dst.frames[rank - 1 :]
    n_sampled = dvvf.frames[rank - 1 :]
    dvvf.dv_s.set_shape(*dst.shape)
    dv_s.set_shape(*dst.shape)
    dv = new_dv(rank, dv_s)
    for m, n in tqdm(list(zip(m_sampled, n_sampled))):
        for _, _n in enumerate(range(n - rank + 1, n + 1)):
            dv[_] = dvvf.get(_n)
        dv /= 255.0
        syn = lf_synthesis_g(dv, dv_s).get()
        syn = (syn * 255).astype(np.uint8)
        dst.save(n, syn)


def lf_fusionsyn(dv_s: DvStruct, dvvf: DV_VideoField, dst: MV_VideoField):
    # syn & evaluate
    dv_s = deepcopy(dv_s)
    rank = dv_s.rank
    n_sampled = dvvf.frames[rank:]
    dvvf.dv_s.set_shape(*dst.shape)
    dv_s.set_shape(*dst.shape)
    FullS = len(dvvf.frames)
    dv = np.empty(((FullS, len(dv_s.layers)) + dv_s.frame_shape)).astype(dtype)
    for idx, n in enumerate(dvvf.frames):
        dv[idx] = dvvf.get(n)
    dv /= 255.0
    filters = [
        [7 / 8.0, 1, 1, 1, 1 / 8.0],
        [6 / 8.0, 1, 1, 1, 2 / 8.0],
        [5 / 8.0, 1, 1, 1, 3 / 8.0],
        [4 / 8.0, 1, 1, 1, 4 / 8.0],
        [3 / 8.0, 1, 1, 1, 5 / 8.0],
        [2 / 8.0, 1, 1, 1, 6 / 8.0],
        [1 / 8.0, 1, 1, 1, 7 / 8.0],
        [0, 1, 1, 1, 1],
    ]
    filters = [np.array(_) for _ in filters]
    filters = [_ / _.sum() for _ in filters]
    dst.folder.mkdir(parents=True, exist_ok=True)
    for n in tqdm(n_sampled):
        for fid in range(8):
            n_idx = n + fid / 8.0
            target_frame = lf_synthesis_g(dv[n - 4 : n + 1], dv_s, filters[fid]).get()
            cv.imwrite(str(dst.folder / f"{n_idx}.png"), target_frame[4, 4] * 255)


def run_cs():
    print("Run color split")
    color_split(mv_shape, origin_folder, cs_folder, frames, offset)


def run_ff():
    print(f"Run FF on {fac_folder}")
    dst_frames = deepcopy(frames)
    # print(retarget_frames)
    for cIdx, ch in enumerate(chs):
        mvvf = MV_VideoField(
            cs_folder / f"ch{ch}", mv_shape, 1
        )  # light field video field
        dvvf = DV_VideoField(fac_folder / f"ch{ch}", dv_s)
        mvvf.frames = retarget_frames
        dvvf.frames = dst_frames
        if cIdx > 0:
            print(f'src: {str(fac_folder / f"ch{chs[0]}")}')
            copy_tree(str(fac_folder / f"ch{chs[0]}"), str(fac_folder / f"ch{ch}"))
            _ntf_s = NTFStruct((1, ntf_s.iteration[1] // 4))
            vntf_ff(dv_s, _ntf_s, mvvf, dvvf, cached=True)
        else:
            _ntf_s = deepcopy(ntf_s)
            vntf_ff(dv_s, _ntf_s, mvvf, dvvf)


def run_brr():
    print(f"Run BRR on {fac_folder}")
    dst_frames = deepcopy(frames)
    factorizeFilter = np.array(
        [0, 0.5, 1.5, 2.5, 3.5, 3.5, 2.5], dtype=np.float16
    )  # brr_filter(rank)
    factorizeFilter /= factorizeFilter.sum()
    for cIdx, ch in enumerate(chs):
        mvvf = MV_VideoField(
            cs_folder / f"ch{ch}", mv_shape, 1
        )  # light field video field
        dvvf = DV_VideoField(fac_folder / f"ch{ch}", dv_s)
        mvvf.frames = retarget_frames
        dvvf.frames = dst_frames
        if cIdx > 0:
            print(f'Init with src: {str(fac_folder / f"ch{chs[0]}")}')
            copy_tree(str(fac_folder / f"ch{chs[0]}"), str(fac_folder / f"ch{ch}"))
            _ntf_s = NTFStruct((1, ntf_s.iteration[1]))
            vntf_rrff(dv_s, _ntf_s, mvvf, dvvf, factorizeFilter, cached=True)
        else:
            _ntf_s = deepcopy(ntf_s)
            vntf_rrff(dv_s, _ntf_s, mvvf, dvvf, factorizeFilter)


def run_syn():
    print("Run synthesis")
    dst_frames = deepcopy(frames)
    for ch in chs:
        mvvf = MV_VideoField(
            syn_folder / f"ch{ch}", mv_shape, 1
        )  # light field video field
        dvvf = DV_VideoField(fac_folder / f"ch{ch}", dv_s)
        mvvf.frames = retarget_frames
        dvvf.frames = dst_frames
        # vntf_ff(dv_s, ntf_s, mvvf, dvvf)
        lf_synthesis(dv_s, mvvf, dvvf)


def run_tfc():
    print(f"Run TFC on {fac_folder}")
    dst_frames = deepcopy(frames)
    # print(retarget_frames)
    ai, factorizeFilter = genFacFilter(dv_s.rank, 1, causal=True, flip=True)
    bi, ObjFilter = genObjFilter(dv_s.rank, 1, causal=True, flip=False)
    # factorizeFilter = np.array([0, 1, 1, 1, 1.0])
    # factorizeFilter = np.array([1.3125,4.5,4.5,4.5,3.1875])
    print(factorizeFilter)
    # factorizeFilter /= factorizeFilter[-1]
    for cIdx, ch in enumerate(chs):
        mvvf = MV_VideoField(
            cs_folder / f"ch{ch}", mv_shape, 1
        )  # light field video field
        dvvf = DV_VideoField(fac_folder / f"ch{ch}", dv_s)
        mvvf.frames = dst_frames
        dvvf.frames = dst_frames
        if cIdx > 0:
            print(f'Init with src: {str(fac_folder / f"ch{chs[0]}")}')
            copy_tree(str(fac_folder / f"ch{chs[0]}"), str(fac_folder / f"ch{ch}"))
            _ntf_s = NTFStruct((1, ntf_s.iteration[1]))
            vntf_rrsf(
                dv_s, _ntf_s, mvvf, dvvf, factorizeFilter, cont=hfr_MV, cached=True
            )
        else:
            _ntf_s = deepcopy(ntf_s)
            vntf_rrsf(dv_s, _ntf_s, mvvf, dvvf, factorizeFilter, cont=hfr_MV)


def run_tf():
    print("Run temporal fusion")
    dst_frames = deepcopy(frames)
    # print(retarget_frames)
    ai, factorizeFilter = genFacFilter(dv_s.rank, 1, causal=False, flip=False)
    bi, ObjFilter = genObjFilter(dv_s.rank, 1, causal=False, flip=False)
    factorizeFilter = np.array(factorizeFilter)
    ObjFilter = np.array(ObjFilter)
    for ch in chs:
        mvvf = MV_VideoField(
            cs_folder / f"ch{ch}", mv_shape, 1
        )  # light field video field
        dvvf = DV_VideoField(fac_folder / f"ch{ch}", dv_s)
        mvvf.frames = retarget_frames
        dvvf.frames = dst_frames
        vntf_sf(dv_s, ntf_s, mvvf, dvvf, [ObjFilter, factorizeFilter], cont=hfr_MV)


def pre_filter(
    sampled_frames,
    mvvf: MV_VideoField,
    dstMVvf: MV_VideoField,
    filterB: np.ndarray,
    block,
):
    ## Pre-Filter
    # seg_frames = sampled_frames[:-(buffer_len-1)]
    segLen = 16
    blen = len(filterB)
    segs = list(range(0, len(sampled_frames) - (blen - 1), segLen - (blen - 1)))
    filterB = np.expand_dims(cp.array(filterB), axis=tuple(range(1, 4 + 1)))
    bufferC = np.empty((segLen,) + dv_s.base_shape, dtype=np.uint8)
    bufferG = cp.empty((segLen,) + dv_s.base_shape, dtype=dtype)
    blkList = generate_blks(dv_s.x_shape, block)
    mvpad = ((0, 0),) * 3 + ((dv_s.offset[0],) * 2,) + ((dv_s.offset[1],) * 2,)

    def _save(dstVF: MV_VideoField, dIdx, data):
        dstVF.saveBlk(dIdx, data, blkList)

    def _filterOnGPU(bufferC, cur_frames, filterB, blen):
        bufferG = cp.array(bufferC)
        cur_frames = cur_frames[: -(blen - 1)]
        for idx, mn in enumerate(cur_frames):
            bufferG[idx] = (bufferG[idx : idx + blen] * filterB).sum(axis=0)
        bufferC = bufferG.get()
        bufferC = np.pad(bufferC, mvpad)
        for idx, mn in enumerate(cur_frames):
            _save(dstMVvf, mn[1], bufferC[idx])

    initFlag = True
    filter_thread = None
    for s_idx in tqdm(segs):
        cur_frames = sampled_frames[s_idx : s_idx + segLen]
        bufferC = Parallel(n_jobs=8)(delayed(mvvf.get)(mn[0]) for mn in cur_frames)
        if not initFlag:
            filter_thread.join()
        filter_thread = threading.Thread(
            target=_filterOnGPU,
            args=(bufferC, cur_frames, filterB, blen),
        )
        filter_thread.start()
        initFlag = False
    filter_thread.join()
    # Parallel(n_jobs=8)(
    #     delayed(_save)(dstMVvf, mn[1], bufferC[idx])
    #     for idx, mn in enumerate(cur_frames)
    # )




def debugIMW(name, img):
    img = (img * 255).astype(np.uint8)
    cv.imwrite(name, img)


def color_merge(src: Path, dst: Path, dv_s: DvStruct, frames: list[int]):
    dv_s.set_rank(1)
    outDv = DV_VideoField(dst, dv_s)
    dvvfs = [DV_VideoField(src / f"ch{ch}", dv_s) for ch in chs]
    container = np.zeros(dv_s.dv_shape + (3,), np.uint8)
    for n in tqdm(frames):
        for ch, dvvf in zip(chs, dvvfs):
            container[..., ch] = dvvf.get([n])
        outDv.save([n], container, 1)


def lf_fusion_synthesis(dv_s: DvStruct, dst: MV_VideoField, dvvf: DV_VideoField):
    # Synthesis
    dv_s = deepcopy(dv_s)
    rank = dv_s.rank
    m_sampled = dst.frames[rank - 1 :]
    n_sampled = dvvf.frames[rank - 1 :]
    dvvf.dv_s.set_shape(*dst.shape)
    dv_s.set_shape(*dst.shape)
    dv = new_dv(rank, dv_s)
    for m, n in tqdm(list(zip(m_sampled, n_sampled))):
        for _, _n in enumerate(range(n - rank + 1, n + 1)):
            dv[_] = dvvf.get(_n)
        dv /= 255.0
        syn = lf_synthesis_g(dv, dv_s).get()
        syn = (syn * 255).astype(np.uint8)
        dst.save(n, syn)


def run_color_merge():
    print(f"Run color merge on {fac_folder}")
    dv_s.set_shape(*mv_shape)
    color_merge(fac_folder, fac3_folder, dv_s, frames)


AVAILABLE_ACTIONS = [
    "run_ff",
    "run_cs",
    "run_brr",
    "run_color_merge",
    "run_tf",
    "run_tfc",
]
runDict = []


def run_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--workspace",
        type=str,
        default="path-to-workspace-folder",
        help="Path to the workspace.",
    )
    parser.add_argument(
        "--ds_name",
        type=str,
        default="wolf_256",
        help="Name of the dataset.",
    )
    parser.add_argument(
        "--Dst",
        type=str,
        default="FF",
        help="The destination folder for the output.",
    )
    parser.add_argument(
        "--ordered_colors",
        type=int,
        nargs=3,
        default=[1, 0, 2],
        help="List of ordered colors.",
    )
    parser.add_argument(
        "--frames",
        type=int,
        nargs=2,
        default=[0, 100],
        help="range of frames begin and end+1.",
    )
    parser.add_argument(
        "--hfr_MV",
        action="store_true",
        help="Whether to use HFR MV.",
    )
    parser.add_argument(
        "--mv_shape",
        type=int,
        nargs=4,
        default=[9, 9, 256, 256],
        help="Shape of the MV.",
    )
    parser.add_argument(
        "--iteration_number",
        type=int,
        default=[1, 200],
        nargs=2,
        help="Number of iterations.",
    )
    parser.add_argument(
        "--rank",
        type=int,
        default=4,
        help="The rank of the matrix.",
    )
    parser.add_argument(
        "--layers",
        type=int,
        nargs=2,
        default=[0, 1],
        help="List of layers.",
    )
    parser.add_argument(
        "--runFilter",
        action="store_true",
        help="Whether to run the filter.",
    )
    parser.add_argument(
        "--prep_dv",
        action="store_true",
        help="Whether to prepare data for DV.",
    )
    parser.add_argument(
        "--action",
        choices=AVAILABLE_ACTIONS,
        help="The action to perform.",
    )
    args = parser.parse_args()
    arg_list = vars(args)
    print(arg_list)
    return arg_list


if __name__ == "__main__":
    args = run_parser()
    frames = list(range(args["frames"][0], args["frames"][1]))
    retarget_frames = [f % 100 for f in frames]
    print(retarget_frames)
    cs_folder = Path(f"{args['workspace']}/Dataset_CS/{args['ds_name']}")
    origin_folder = Path(f"{args['workspace']}/{args['ds_name']}")
    fac_folder = Path(f"{args['workspace']}/Result/Fac/{args['Dst']}/{args['ds_name']}")
    fac3_folder = Path(
        f"{args['workspace']}/Result/Fac3/{args['Dst']}/{args['ds_name']}"
    )
    syn_folder = Path(f"{args['workspace']}/Result/Syn/{args['Dst']}/{args['ds_name']}")
    offset = 0
    mv_shape = args["mv_shape"]
    ntf_s = NTFStruct(args["iteration_number"])
    dv_s = DvStruct(args["layers"], args["rank"], 1, args["mv_shape"])
    chs = args["ordered_colors"]
    hfr_MV = args["hfr_MV"]
    runFilter = args["runFilter"]
    prep_dv = args["prep_dv"]
    globals()[args["action"]]()
