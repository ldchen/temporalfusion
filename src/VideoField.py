#%%
from pathlib import Path
import json
from logging import raiseExceptions
from MvField import MvField
from DvStruct import DvStruct
import numpy as np
import cupy as cp
import cv2 as cv
import config
from config import INFO, DEBUG

from lf_fact import setup_dv


def mask_dv(dv, dv_s: DvStruct):        
    of_vert, of_hori = dv_s.offset
    vx, hx = dv_s.x_shape
    vs, hs = dv_s.step
    # Mask boundaries
    dv[:, :, :of_vert, :] = 0
    dv[:, :, :, :of_hori] = 0
    for l in dv_s.layers:
        l_idx, l_pos = l
        # print(l, hs, vx+of_vert+l*vs)
        dv[:, l_idx, vx + of_vert + l_pos * vs :, :] = 0
        dv[:, l_idx, :, hx + of_hori + l_pos * hs :] = 0

def new_dv(frames, dv_s, dtype=np.float16) -> np.ndarray:
    # prepare dv
    dv = np.random.rand(frames, *dv_s.dv_shape[1:]).astype(dtype)
    mask_dv(dv, dv_s)
    return dv 

class VideoField:
    def __init__(self, path, step=None, type="mv", mode=cv.IMREAD_GRAYSCALE) -> None:
        self.folder = Path(path)
        # self.sequence_end = self.config.get("sequence_end")
        self.step = step
        self.type = type
        self.load_mode = mode

    def load_video_field(self):
        print(f'Config Path: {self.folder / "config.json"}')
        with open(self.folder / "config.json", "r") as js_file:
            config = json.load(js_file)
        return config

    def save_frame(self, folder, method, data=None, prefix=None):
        if data is None:
            data = self.data
        method(data, folder, prefix)

    def frame_calibration(self, rank):
        self.frames = [
            f + (rank - 1) for f in self.frames if f + (rank - 1) < self.sequence_end
        ]

    def set_frames(self, video_range=None):
        if video_range is None:
            self.sequence_end = self.config["sequence_end"] + 1
            self.frames = list(
                range(
                    self.config["sequence_start"],
                    self.config["sequence_end"] + 1,
                    self.config["sequence_steps"],
                )
            )
        else:
            self.sequence_end = video_range[1] + 1
            self.frames = list(
                range(
                    video_range[0],
                    video_range[1] + 1,
                    video_range[2],
                )
            )

    def load_mv_config(self):
        if "view_vert" in self.config.keys():
            _mv_shape = (
                self.config["view_vert"],
                self.config["view_hori"],
                self.config["res_vert"],
                self.config["res_hori"],
            )
            if self.load_mode == cv.IMREAD_COLOR:
                _mv_shape = _mv_shape + (3,)
        else:
            _mv_shape = tuple(self.config["base_shape"])
        if self.step is not None:  # override sequence step if given
            self.config["sequence_steps"] = self.step
        self.mv_shape = _mv_shape
        self.data = np.empty(self.mv_shape, dtype=config.dtype)

    def load_dv_config(self):
        self.dv_shape = tuple(self.config["dv_shape"])
        self.frames = self.config["frames"]
        self.rank = self.config["rank"]
        self.layers = self.config["layers"]

    def load_img_config(self):
        self.img_shape = self.config["base_shape"][2:]
        self.frames = self.config["frames"]

    def load_mv_merged(self, idx, buffer: cp.array = None):
        # Load mv frame from disk
        # single channel image is expected
        folder = self.folder / f"{idx:06}"
        INFO(f"Loading frame {idx} from {folder}")

        def _load_mv(dst):
            vshape = self.mv_shape[:2]
            for vv, hv in np.ndindex(vshape):
                view_name = f"cam_{vv}_{hv}.png"
                filename = folder / view_name
                dst[vv, hv] = cv.imread(str(filename), self.load_mode) / 255.0
            return dst
            
        buffer = self.data if buffer is None else buffer
        # pad = [p * self.dv_s.max_interval for p in self.dv_s.step]
        # buffer_roi = buffer[..., pad[0]:-pad[0], pad[1]:-pad[1]]
        _load_mv(buffer)
        return buffer

    def load_dv_content(self, idx, buffer=None):
        # Load mv frame from disk
        # single channel image is expected
        def _load_dv(dst):
            for l in self.dv_s.layers:
                l_idx, l_pos = l
                if idx < 0:
                    layer_name = f"dv_{l_idx}_{idx:07}.png"
                else:
                    layer_name = f"dv_{l_idx}_{idx:06}.png"
                filename = self.folder / layer_name
                # if config.DEBUG:
                #     print(f"Loading frame {idx} from {filename}")
                if filename.exists():
                    # print(f'load_dv: {idx} layer {l_idx}')
                    dst[l_pos] = cv.imread(str(filename), self.load_mode) / 255.0
                else:
                    print(f"\t init idx {idx} layer {l_idx}")
                    dst[l_pos] = _setup_dv(dst, l_pos)
            return dst

        def _setup_dv(dst, l_pos):
            # initialize dv and mask boundaries
            tmp = np.random.rand(*dst[l_pos].shape).astype(np.float32)
            dv_s = self.dv_s
            of_vert, of_hori = dv_s.offset
            vx, hx = dv_s.x_shape
            vs, hs = dv_s.step
            # Mask boundaries
            tmp[:of_vert, :] = 0
            tmp[:, :of_hori] = 0
            # print(l, hs, vx+of_vert+l*vs)
            tmp[vx + of_vert + l_pos * vs :, :] = 0
            tmp[:, hx + of_hori + l_pos * hs :] = 0
            return tmp

        if buffer is None:
            buffer = self.data
        if hasattr(buffer, "device"):
            tmp = _load_dv(np.empty(buffer.shape, dtype=np.float32))
            buffer.set(tmp)
        else:
            _load_dv(buffer)
        return buffer

    def load_img_content(self, idx, buffer=None):
        pass



def _load_seperated(mvv, dst, path):
    vshape = mvv.mv_shape[:2]
    for vv, hv in np.ndindex(vshape):
        view_name = f"cam_{vv}_{hv}.png"
        filename = path / view_name
        dst[vv, hv] = cv.imread(str(filename)) / 255.0
    return dst
def _load_merged(mvv, dst, path):
    mv_shape = mvv.mv_shape[:4]
    src = cv.imread(str(path), cv.IMREAD_UNCHANGED) / 255.0 
    vv, hv, vx, hx = mv_shape
    for v, h in np.ndindex((vv, hv)):
        dst[v, h] = src[v*vx:(v+1)*vx, h*vx:(h+1)*hx]
    return dst

class MV_VideoField(VideoField):
    def __init__(self, path, dv_s: DvStruct=None, step=None, ftype='merged', mode=cv.IMREAD_GRAYSCALE):
        super().__init__(path, mode=mode)
        self.config = self.load_video_field()
        self.sequence_end = self.config.get("sequence_end")
        self.step = step
        self.load_mv_config()
        self.in_buffer = []
        self.dv_s = dv_s
        self.ftype = ftype
        self.load = _load_merged if ftype=='merged' else _load_seperated
        self.isPad = False
        self.data: np.ndarray = None
        if self.dv_s is not None:
            self.isPad = True
            self.dv_s.set_shape(*(self.mv_shape[:4]))

    def setup(self, cur_frame=None):
        # Prepare Buffer
        buffer_len = 100
        buffer_shape = (buffer_len,)
        if self.dv_s is None:
            # buffer shape fit exact as input
            buffer_shape += self.mv_shape
        else:
            buffer_shape += self.dv_s.mv_shape
        if self.data is None:
            self.data = np.zeros(buffer_shape, dtype=config.dtype)
        if cur_frame is None:
            cur_frame = self.frames[0]
        try:
            idx = self.frames.index(cur_frame)
        except ValueError:
            DEBUG("MV Frame index out of range")
            return
        end = idx+buffer_len
        if end > self.frames[-1]:
            end = len(self.frames)
        frames_to_load = self.frames[idx:end]
        # print(f"Frames to load: {frames_to_load}")
        for idx, n_idx in enumerate(frames_to_load):
            self.load_mv(n_idx, self.data[idx])
        self.in_buffer = frames_to_load
    
    # def setup_pure(self, cur_frame=None):
    #     buffer_len = 32
        
    #     if cur_frame is None:
    #         cur_frame = self.frames[0]
    #     try:
    #         idx = self.frames.index(cur_frame)
    #     except IndexError:
    #         DEBUG("Frame index out of range")
    #         return
    #     frames_to_load = self.frames[idx:idx+buffer_len]
    #     for i, f in enumerate(frames_to_load):
    #         self.load_mv_pure(f, self.data[i])
    #     self.in_buffer = frames_to_load
    def load_mv(self, idx, buffer: np.array = None):
        buffer = self.data if buffer is None else buffer
        if self.isPad:
            pad = [p * self.dv_s.max_interval for p in self.dv_s.step]
            buffer_roi = buffer[:, :, pad[0]:-pad[0], pad[1]:-pad[1]]
        else:
            buffer_roi = buffer
        if self.ftype == 'merged':
            dst_path = self.folder / f"{idx:06}.png"
        else:
            dst_path = self.folder / f"{idx:06}"
        self.load(self, buffer_roi, dst_path)
    
            
    def get(self, frame, n=1):
        try:
            idx = self.in_buffer.index(frame)
        except ValueError:
            self.setup(frame)
            idx = 0
        return self.data[idx]

class DV_VideoField(VideoField):
    def __init__(self, path, dv_s:DvStruct, step=None) -> None:
        VideoField.__init__(self, path, step)
        self.dv_s = dv_s
        try:
            config = self.load_video_field()
            self.config = config
            self.dv_s.set_shape(*config['base_shape'])
        except:
            DEBUG("Config file not exist")
            self.config = self.dv_s.toDict()
        self.prefix = 0
        self.frame_idx = 0
        self.in_buffer = []
        self.cached = False

    def save_frames(self, frames):
        """Save frames
        Assume consective frames
        """
        layers = self.dv_s.layers
        folder = self.folder
        # frames = self.in_buffer
        for f in frames:
            data_idx = self.in_buffer.index(f)
            for l in layers:  # rank, layer
                view_name = f"dv_{l[0]}_{f:06}.png"
                filename = folder / view_name
                INFO(f"Write data {data_idx} to {filename}")
                cv.imwrite(str(filename), (self.data[data_idx, l[0]]*255).astype(np.uint8))

    def create_folder(self):
        INFO(f"Create folder: {self.folder}")
        self.folder.mkdir(parents=True, exist_ok=True)
        with open(self.folder / "config.json", "w") as js_file:
            dv_config = self.dv_s.toDict()
            dv_config.update({"frames": self.frames})
            json.dump(dv_config, js_file)
            
    def load_frames(self, frame, container):
        folder = self.folder
        # if DEBUG:
        #     print(f"Loading frame {idx} from {folder}")
        for l in self.dv_s.layers:
            l_ = l[1]
            name = f"dv_{l_}_{frame:06}.png"
            filename = folder / name
            # print(filename)
            container[l_] = cv.imread(str(filename), cv.IMREAD_UNCHANGED) / 255.0
        return container

    def setup(self, cur_frame=None):
        buffer_len = 100
        if cur_frame is None:
            cur_frame = self.frames[0]
        try:
            idx = self.frames.index(cur_frame)
        except ValueError:
            DEBUG("DV Frame index out of range")
            return
        frames_to_load = self.frames[idx:idx+buffer_len]
        if self.cached:
            INFO(f"Use cached content from disk {frames_to_load}")
            data = new_dv(buffer_len, self.dv_s)
            for i, f in enumerate(range(cur_frame, cur_frame+buffer_len)):
                try:
                    idx = frames_to_load.index(f)
                    self.load_frames(f, data[i])
                except ValueError:
                    pass
        else:
            INFO(f"Create new dv for {frames_to_load}")
            data = new_dv(buffer_len, self.dv_s)
            for i, f in enumerate(self.in_buffer):
                try:
                    idx = frames_to_load.index(f)
                    INFO(f"Reuse frame {f} from {i} -> {idx}")
                    data[idx] = self.data[i] 
                except ValueError:
                    pass
        self.data = data
        self.in_buffer = frames_to_load

    def get(self, frame, n=1):
        reload = False
        idx_s = 0
        try:
            idx_s = self.in_buffer.index(frame)
        except ValueError:
            reload = True
        try:
            idx_e = self.in_buffer.index(frame+n-1)
        except ValueError:
            reload = True
        if reload:
            idx_s = 0
            self.setup(frame)
        INFO(f"get {idx_s} to {idx_s+n}")
        return self.data[idx_s:idx_s+n]

if __name__ == "__main__":
    pass
