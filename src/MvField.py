import cv2 as cv
import numpy as np
import json
from pathlib import Path
import os

def load_blender(path, shape):
    prefix = 'input_Cam'
    suffix = '.png'
    name = 'blender'
    vshape = shape[:2]
    buffer = np.zeros(shape, dtype=np.uint8)
    for vv, hv in np.ndindex(vshape):
        idx = vv * vshape[1] + hv
        mapped_name = f'{idx:03}'
        filename = ''.join([prefix, mapped_name, suffix])
        filename = Path(path)/filename
        buffer[vv, hv] = cv.imread(str(filename))
    return buffer/255.0

def mv_depad(mv, pad):
    vx = mv.shape[2] - pad[0]*2
    hx = mv.shape[3] - pad[1]*2
    return mv_crop_center(mv, (vx, hx))

def mv_crop_center(mv_3, shape):
    vx, hx = mv_3.shape[2:4]
    nv = (vx - shape[0])//2
    nh = (hx - shape[1])//2
    return mv_3[:, :, nv:vx-nv, nh:hx-nh]

def convert2_8bit_depth(src_img: np.ndarray):
    return (src_img*255).astype(np.uint8)

def merge_mv(src_mv:np.ndarray, dst_mv=None):
    vv, hv, vx, hx = src_mv.shape[:4]
    if dst_mv is None:
        if len(src_mv.shape) == 4:
            dst_shape = (vv*vx, hv*hx)
        else:
            dst_shape = (vv*vx, hv*hx, src_mv.shape[4])
        dst_mv = np.empty(dst_shape, dtype=src_mv.dtype)
    for v, h in np.ndindex((vv, hv)):
        dst_mv[v*vx:(v+1)*vx, h*vx:(h+1)*hx] = src_mv[v, h]
    return dst_mv

class MvField:
    data: np.ndarray

    def __init__(self, mv_shape=None, file=None, data=None):
        if file:
            self.mv_load(file)
        elif mv_shape:
            self.data = np.empty(shape=mv_shape)
        elif data is not None:
            self.data = data

    @property
    def shape(self):
        return self.data.shape

    @classmethod
    def load(cls, file, idx):
        file = os.path.join(file, f'{idx:06}')
        mv = MvField(file=file)
        return mv.data

    def mv_load(self, folder_path):
        with open(os.path.join(folder_path, 'config.json'), 'r') as js_file:
            config = json.load(js_file)
        shape = (config ['view_vert'], config['view_hori'],
                 config['res_vert'],   config['res_hori'],
                 config['channel'])
        config['path'] = folder_path
        config['shape'] = shape
        self.data = load_blender(folder_path, shape)
        return self

    def disp_shift(self, one_view_disp=2):
        disp = one_view_disp * (self.shape[0] // 2)
        shape = list(self.shape)
        img_shape = shape[2:4]
        adisp = abs(disp)
        img_shape[0] -= adisp * 2
        img_shape[1] -= adisp * 2
        new_shape = shape[0:2] + img_shape + shape[4:]
        result = MvField(new_shape)
        for i, j in np.ndindex(tuple(new_shape[0:2])):
            i_phy, j_phy = i - self.shape[0] // 2, j - self.shape[1] // 2
            offset = np.float32(
                [[1, 0, j_phy * one_view_disp - adisp], [0, 1, i_phy * one_view_disp - adisp]])
            cv.warpAffine(self.data[i, j], offset, (new_shape[3], new_shape[2]), result.data[i, j])
        # result.data = result.data[:, :, 
                                #   0 + adisp*2:new_shape[2]-adisp*2, 0+adisp*2:new_shape[3]-adisp*2, :]
        return result

if __name__ == '__main__':
    a = MvField(file='..//Dataset/rosemary/')
    print(a.shape)