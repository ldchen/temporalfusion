import numpy as np
from numpy.lib.stride_tricks import as_strided


def prod(li):
    result = li[0].copy()
    for _ in li[1:]:
        result *= _
    return result


def mul_update(tensor, t_vec, q_vecs, plane, sum_axis):
    q_vec = prod(q_vecs)
    t_aprox = (t_vec * q_vec).sum(axis=0)
    num = (tensor * q_vec).sum(axis=sum_axis)
    den = (t_aprox * q_vec).sum(axis=sum_axis) + 1E-5
    plane *= num/den
    plane.clip(0, 1, out=plane)


def synthesize(vecs, weight=None):
    rank_tensor = prod(vecs)
    if weight is None:
        t_approx = rank_tensor.sum(axis=0)
    else:
        t_approx = weight.dot(rank_tensor)
    return t_approx


def nmf2D(m, rank, iter=100):
    shape = m.shape
    f = np.random.rand(rank, shape[0])  # r, p
    g = np.random.rand(rank, shape[1])  # r, q
    ds = m.itemsize
    # m_tensor: 1, p, q
    m_tensor = as_strided(m, (1,)+shape, (0, shape[1]*ds, ds))
    ds = f.itemsize
    # mf, mg: r, p, q
    mf = as_strided(f, (rank,)+shape, (shape[0]*ds, ds, 0))
    mg = as_strided(g, (rank,)+shape, (shape[1]*ds, 0, ds))
    for _ in range(iter):
        # f <- (m_tensor * mg)_q / ((mf * mg)_r * mg)_q
        mul_update(m_tensor, mf, (mg,), f, (2))
        # g <- (m_tensor * mf)_p / ((mf * mg)_r * mp)_p
        mul_update(m_tensor, mg, (mf,), g, (1))
    return f, g


def nmf2D_example():
    m = np.random.rand(10, 4) @ np.random.rand(4, 10)
    f, g = nmf2D(m, 4, 1000)
    mat_a = f.T @ g
    return abs(m-mat_a).mean()


if __name__ == '__main__':
    print(nmf2D_example())
