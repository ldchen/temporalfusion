import cupy as cp


def get_quotients(l_ref, layers):
    result = []
    for l_idx, l_tgt in enumerate(layers):
        if l_ref != l_tgt:
            result.append(l_idx)
    return tuple(result)


class DvStruct:
    def __init__(self, layer_list, rank, max_interval, lf_shape=None):
        self.layers = list(enumerate(layer_list))  # layer index, layer location
        self.rank = rank
        self.max_interval = max_interval
        if lf_shape is not None:
            self.set_shape(*lf_shape)
        self.quotient = [get_quotients(_, layer_list) for _ in layer_list]

    def update(self):
        self.set_shape(*(self.v_shape + self.x_shape))

    def set_shape(self, vv, hv, vx, hx):
        """Setup dv shape from mv_shape and dv_s

        Args:
            mv_shape (tuple): (vv, hv, vx, hx)
        """
        # ch, vv, hv, vx, hx = mv_shape
        v_shape = (vv, hv)
        x_shape = (vx, hx)
        d_shape = (self.rank, len(self.layers))
        self.base_shape = (vv, hv, vx, hx)
        self.step = (vv - 1), (hv - 1)
        self.offset = tuple(step * self.max_interval for step in self.step)
        self.frame_shape = tuple(
            x + step * self.max_interval * 2 for x, step in zip(x_shape, self.step)
        )
        self.mv_shape = v_shape + self.frame_shape
        self.dv_shape = d_shape + self.frame_shape
        self.x_shape = x_shape
        self.v_shape = v_shape
        self.d_shape = d_shape


    def toDict(self):
        return {
            "layers": self.layers,
            "rank": self.rank,
            "base_shape": self.base_shape,
            "dv_shape": self.dv_shape,
        }

    def set_rank(self, rank):
        self.rank = rank
        self.update()
        return self

    def setup_weight(self, weight):
        wsum = sum(weight)
        # self.weight = cp.array(weight, dtype=dtype)/wsum
        # self.weight5 = self.weight.reshape((-1,) + (1,)*4)
        # self.weight6 = self.weight.reshape((-1,) + (1,)*5)
        self.wsum = 1

    def __repr__(self):
        result = ""
        result += f"(l, r, i): {(self.layers, self.rank, self.max_interval)}\n"
        result += f"offset: {self.offset}\n"
        result += f"frame_shape: {self.frame_shape}\n"
        result += f"mv_shape: {self.mv_shape}\n"
        result += f"dv_shape: {self.dv_shape}\n"
        result += f"base_shape: {self.base_shape}"
        return result
