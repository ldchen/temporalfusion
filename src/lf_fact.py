#
# %%
from lff.MvField import MvField
from lff.DvStruct import DvStruct
from pathlib import Path
import numpy as np
import cupy as cp
import cv2 as cv

# %%


class NTFStruct:
    def __init__(self, iteration, block=None):
        self.iteration = iteration
        self.block = block

    def __repr__(self):
        result = ""
        result += f"iteration: {self.iteration}\n"
        result += f"block: {self.block}\n"
        if self.b:
            result += f"b[n]: {self.b}\n"
        if self.a:
            result += f"a[n]: {self.a}\n"
        return result


def setup_mv(mv, dv_s) -> np.ndarray:
    # setup padding
    of_vert, of_hori = dv_s.offset
    xp = cp.get_array_module(mv)
    mv_pad = xp.pad(mv, ((0, 0), (0, 0), (of_vert, of_vert), (of_hori, of_hori)))
    return mv_pad


def setup_dv(dv_s, dtype=np.float16) -> np.ndarray:
    # prepare dv
    dv = np.random.rand(*dv_s.dv_shape).astype(dtype)
    dv += 0.2
    of_vert, of_hori = dv_s.offset
    vx, hx = dv_s.x_shape
    vs, hs = dv_s.step
    # Mask boundaries
    dv[:, :, :of_vert, :] = 0
    dv[:, :, :, :of_hori] = 0
    for l in dv_s.layers:
        l_idx, l_pos = l
        # print(l, hs, vx+of_vert+l*vs)
        dv[:, l_idx, vx + of_vert + l_pos * vs :, :] = 0
        dv[:, l_idx, :, hx + of_hori + l_pos * hs :] = 0
    return dv


# %%


def tensor_to_tensor_view(mv, src_layer, dv_s, blk_anchor=(0, 0), blk_shape=None):
    """mv_0 -> mv_l
    mv_l(v, x) = mv_0(v, v*d+x), where d = 0 - l;
    mv_l(v, x) = mv[vv, hv, vv*d+vx, hv*d+hx]
    """
    xp = cp.get_array_module(mv)
    if blk_shape is None:
        blk_shape = dv_s.x_shape
    offset = tuple(of + anchor for of, anchor in zip(dv_s.offset, blk_anchor))
    src_layer = src_layer[1]
    x = tuple(_x + step * src_layer for _x, step in zip(blk_shape, dv_s.step))
    dsize = mv.itemsize
    d = 0 - src_layer
    vv, hv = v_shape = dv_s.v_shape
    h, w = dv_s.frame_shape
    # stride change(vert view, hori view, vert pixel, hori pixel)
    stride = (
        hv * h * w
        # d(mv)/d(vv) = mv[1, 0, d, 0] = h_view*height*width + d*width
        + d * w,
        h * w + d,  # d(mv)/d(hv) = mv[0, 1, 0, d] = height*width + d
        w,  # d(mv)/d(vx) = mv[0, 0, 1, 0] = width
        1,  # d(mv)/d(hx) = mv[0, 0, 0, 1] = 1
    )
    stride = tuple(s * dsize for s in stride)
    shape = v_shape + x
    result = xp.lib.stride_tricks.as_strided(
        mv[0, 0, offset[0], offset[1] :], shape, stride
    )
    return result


def layer_to_tensor_view(
    planes, src_layer, dst_layer, dv_s, blk_anchor=(0, 0), blk_shape=None
):
    """dv[r, src_layer] -> dv_vx(l_dst)[r, v, x]
    dv_vx(l_dst)[r, v, x] = dv(r, l_dst, v*d+x);
                          = dv[r, l_dst, vv*d+vx, hv*d+hx]
    """
    xp = cp.get_array_module(planes)
    # Block-wise access
    if blk_shape is None:
        blk_shape = dv_s.x_shape
    offset = tuple(of + anchor for of, anchor in zip(dv_s.offset, blk_anchor))
    src_layer = src_layer[1]
    dst_idx, dst_layer = dst_layer
    x = tuple(_x + step * src_layer for _x, step in zip(blk_shape, dv_s.step))
    r, l = dv_s.d_shape
    r = planes.shape[0]
    dsize = planes.itemsize
    d = dst_layer - src_layer  # ray direction
    h, w = dv_s.frame_shape
    # stride change(frame, vert view, hori view, vert pixel, hori pixel)
    stride = (
        l * h * w,  # d(dv)/d(r) = dv1, 0, 0, 0) = layers * height * width
        d * w,  # d(dv)/d(vv) = dv(0, 0, d, 0) = d * width
        d,  # d(dv)/d(hv) = dv(0, 0, 0, d) = d * 1
        w,  # d(dv)/d(vx) = dv(0, 0, 1, 0) = width
        1,
    )  # d(dv)/d(hx) = dv(0, 0, 0, 1) = 1
    stride = tuple(s * dsize for s in stride)
    shape = (r,) + dv_s.v_shape + x
    # stride = rank_step, vv_step, hv_step, vp_step, hp_step
    result = xp.lib.stride_tricks.as_strided(
        planes[0, dst_idx, offset[0], offset[1] :], shape, stride
    )
    return result


def crop_layer(planes, src_layer, dv_s, blk_anchor=(0, 0), blk_shape=None):
    """Get updadant region"""
    if blk_shape is None:
        blk_shape = dv_s.x_shape
    offset = dv_s.offset[0] + blk_anchor[0], dv_s.offset[1] + blk_anchor[1]
    src_idx, src_layer = src_layer
    x = tuple(_x + step * src_layer for _x, step in zip(blk_shape, dv_s.step))
    result = planes[
        :, src_idx, offset[0] : offset[0] + x[0], offset[1] : offset[1] + x[1]
    ]
    return result


# %%


def generate_blks(base_shape, blk_shape):
    anchors_o = [np.arange(0, bs, step) for bs, step in zip(base_shape, blk_shape)]
    anchors_m = np.dstack(np.meshgrid(*anchors_o)).reshape(-1, 2)
    blk_info = np.pad(anchors_m, ((0, 0), (0, 2)))
    for anchor in blk_info:
        for _ in range(2):  # 2 dim
            shape_ = blk_shape[_]
            if shape_ + anchor[_] >= base_shape[_]:
                shape_ = base_shape[_] - anchor[_]
            anchor[_ + 2] = shape_
    # print(blk_info)
    return blk_info


def lf_fact_weight_gg(
    mvg: cp.ndarray,
    dvg: cp.ndarray,
    dv_s: DvStruct,
    weight,
    ntf_s: NTFStruct,
    uidx=None,
):
    l_len = len(dv_s.layers)
    dv_vx = np.empty((l_len,) * 2, dtype=object)
    mv_vx = np.empty((l_len,), dtype=object)
    dv_roi = np.empty((l_len,), dtype=object)
    if uidx is None:
        uidx = slice(0, dvg.shape[0])
    blk_shape = ntf_s.block
    iteration = ntf_s.iteration
    if blk_shape is None:
        blk_shape = dv_s.x_shape
    blks = generate_blks(dv_s.x_shape, blk_shape)
    for oiter in range(iteration[0]):
        for blk in blks:
            blk_anchor, blk_range = blk[:2], blk[2:]
            for l_src in dv_s.layers:  # origin layer
                ls_idx = l_src[0]
                mv_vx[ls_idx] = tensor_to_tensor_view(
                    mvg, l_src, dv_s, blk_anchor, blk_range
                )
                dv_roi[ls_idx] = crop_layer(dvg, l_src, dv_s, blk_anchor, blk_range)
                for l_dst in dv_s.layers:  # destination layer
                    ld_idx = l_dst[0]
                    dv_vx[ls_idx, ld_idx] = layer_to_tensor_view(
                        dvg, l_src, l_dst, dv_s, blk_anchor, blk_range
                    )

            # sum_axis = (0, 1)
            sum_axis = (1, 2)
            for i_iter in range(iteration[1]):
                for l_src, l_quo in zip(dv_s.layers, dv_s.quotient):
                    ls_idx = l_src[0]
                    if len(l_quo) == 1:
                        quotient = dv_vx[ls_idx, l_quo[0]]
                    else:
                        quotient = cp.prod(dv_vx[ls_idx, l_quo])
                    # aprox = dv_vx[ls_idx, ls_idx][0] * quotient[0] * weight[0]
                    # for idx in range(1, 9):
                    # aprox += dv_vx[ls_idx, ls_idx][idx] * quotient[idx] * weight[idx]
                    aprox = ((dv_vx[ls_idx, ls_idx] * quotient) * weight).sum(axis=0)
                    num = (mv_vx[ls_idx] * quotient[uidx]).sum(axis=sum_axis)
                    den = (aprox * quotient[uidx]).sum(axis=sum_axis) + 1e-3
                    dv_roi[ls_idx][uidx] *= num / den
                    dv_roi[ls_idx][uidx].clip(1e-3, 1, out=dv_roi[ls_idx][uidx])
    return dvg


def lf_fact_weight_special(
    mvg: cp.ndarray,
    dvg: cp.ndarray,
    dv_s: DvStruct,
    weight,
    uidx=None,
    blk_shape: tuple = None,
    iteration: tuple = (4, 50),
):
    l_len = len(dv_s.layers)
    dv_vx = np.empty((l_len,) * 2, dtype=object)
    mv_vx = np.empty((l_len,), dtype=object)
    dv_roi = np.empty((l_len,), dtype=object)
    if uidx is None:
        uidx = slice(0, dvg.shape[0])

    if blk_shape is None:
        blk_shape = dv_s.x_shape
    blks = generate_blks(dv_s.x_shape, blk_shape)
    for oiter in range(iteration[0]):
        for blk in blks:
            blk_anchor, blk_range = blk[:2], blk[2:]
            for l_src in dv_s.layers:  # origin layer
                ls_idx = l_src[0]
                mv_vx[ls_idx] = tensor_to_tensor_view(
                    mvg, l_src, dv_s, blk_anchor, blk_range
                )
                dv_roi[ls_idx] = crop_layer(dvg, l_src, dv_s, blk_anchor, blk_range)
                for l_dst in dv_s.layers:  # destination layer
                    ld_idx = l_dst[0]
                    dv_vx[ls_idx, ld_idx] = layer_to_tensor_view(
                        dvg, l_src, l_dst, dv_s, blk_anchor, blk_range
                    )

            # sum_axis = (0, 1)
            sum_axis = (1, 2)
            for i_iter in range(iteration[1]):
                for l_src, l_quo in zip(dv_s.layers, dv_s.quotient):
                    ls_idx = l_src[0]
                    if len(l_quo) == 1:
                        quotient = dv_vx[ls_idx, l_quo[0]]
                    else:
                        quotient = cp.prod(dv_vx[ls_idx, l_quo])
                    aprox = ((dv_vx[ls_idx, ls_idx] * quotient) * weight).sum(axis=0)
                    num = (mv_vx[ls_idx] * quotient[uidx]).sum(axis=sum_axis)
                    den = (aprox * quotient[uidx]).sum(axis=sum_axis) + 1e-3
                    dv_roi[ls_idx][uidx] *= num / den
                    dv_roi[ls_idx][uidx].clip(1e-3, 1, out=dv_roi[ls_idx][uidx])
    return dvg


def lf_fact_gg(
    mvg: cp.ndarray,
    dvg: cp.ndarray,
    dv_s: DvStruct,
    ntf_s: NTFStruct,
    epsilon: float = 1e-5,  # half pixel
):
    l_len = len(dv_s.layers)
    dv_vx = np.empty((l_len,) * 2, dtype=object)
    mv_vx = np.empty((l_len,), dtype=object)
    dv_roi = np.empty((l_len,), dtype=object)
    blk_shape = ntf_s.block
    iteration = ntf_s.iteration
    if blk_shape is None:
        blk_shape = dv_s.x_shape
    blks = generate_blks(dv_s.x_shape, blk_shape)
    for oiter in range(iteration[0]):
        for blk in blks:
            blk_anchor, blk_range = blk[:2], blk[2:]
            for l_src in dv_s.layers:  # origin layer
                ls_idx = l_src[0]
                mv_vx[ls_idx] = tensor_to_tensor_view(
                    mvg, l_src, dv_s, blk_anchor, blk_range
                )
                dv_roi[ls_idx] = crop_layer(dvg, l_src, dv_s, blk_anchor, blk_range)
                for l_dst in dv_s.layers:  # destination layer
                    ld_idx = l_dst[0]
                    dv_vx[ls_idx, ld_idx] = layer_to_tensor_view(
                        dvg, l_src, l_dst, dv_s, blk_anchor, blk_range
                    )

            sum_axis = (1, 2)
            for i_iter in range(iteration[1]):
                for l_src, l_quo in zip(dv_s.layers, dv_s.quotient):
                    ls_idx = l_src[0]
                    if len(l_quo) == 1:
                        quotient = dv_vx[ls_idx, l_quo[0]]
                    else:
                        quotient = cp.prod(dv_vx[ls_idx, l_quo])
                    aprox = (dv_vx[ls_idx, ls_idx] * quotient).sum(axis=0)
                    num = (mv_vx[ls_idx] * quotient).sum(axis=sum_axis)
                    den = (aprox * quotient).sum(axis=sum_axis) + epsilon
                    dv_roi[ls_idx] *= num / den
                    dv_roi[ls_idx].clip(0, 1, out=dv_roi[ls_idx])
    return dvg


def isGPU(x):
    return cp.get_array_module(x).__name__ == "cupy"


def toGPU(x):
    if not isGPU(x):
        return cp.asarray(x)
    else:
        return x


def lf_fact(mv, dv, dv_s, ntf_s: NTFStruct, to_cpu=True):
    mvg = toGPU(mv)
    dvg = toGPU(dv)
    lf_fact_gg(mvg, dvg, dv_s, ntf_s)
    if to_cpu and not isGPU(dv):
        dv[...] = dvg.get()
        return dv
    else:
        return dvg


# %%
def lf_synthesis_g(dv, dv_s, weight=None):
    l_len = len(dv_s.layers)
    dv_vx = np.empty((l_len,), dtype=object)
    dvg = cp.array(dv)
    if weight is None:
        weight = np.ones(dv_s.rank, dtype=float) / dv_s.rank
    _w = cp.expand_dims(cp.array(weight), axis=(1, 2, 3, 4))
    for l_dst in dv_s.layers:  # origin layer
        dst_idx = l_dst[0]
        dv_vx[dst_idx] = layer_to_tensor_view(dvg, (0, 0), l_dst, dv_s)
    a = cp.ones_like(dv_vx[0])
    for l_dst in dv_s.layers:  # origin layer
        dst_idx = l_dst[0]
        a *= dv_vx[dst_idx]
    a = (a * _w).sum(axis=0)
    return a


def lf_synthesis(dv, dv_s):
    l_len = len(dv_s.layers)
    dv_vx = np.empty((l_len,), dtype=object)
    dvg = cp.array(dv)
    for l_dst in dv_s.layers:  # origin layer
        dst_idx = l_dst[0]
        dv_vx[dst_idx] = layer_to_tensor_view(dvg, (0, 0), l_dst, dv_s)
    a = cp.ones_like(dv_vx[0])
    for l_dst in dv_s.layers:  # origin layer
        dst_idx = l_dst[0]
        a *= dv_vx[dst_idx]
    a = a.sum(axis=0) / dv_s.rank
    return a.get()


def psnr(a, b):
    mse = np.mean(np.square(a - b))
    return 20 * np.log10(1 / np.sqrt(mse))


def image_mode_lf_factor_given(
    mv, dv, dv_s, ntf_s: NTFStruct, dtype=np.float16, synthesis=True
):
    mv = mv.astype(dtype)
    dv = lf_fact(mv, dv, dv_s, ntf_s)
    amv = lf_synthesis(dv, dv_s) if synthesis else None
    return dv, amv


def image_mode_lf_factor(mv_img, dv_s, ntf_s, dtype=np.float16, synthesis=True):
    # mv = setup_mv(mv_img, dv_s) * dv_s.rank
    dv: np.ndarray = setup_dv(dv_s, dtype)
    amv: np.ndarray

    mv = mv_img.astype(dtype)
    dv = dv.astype(dtype)
    dv = lf_fact(mv, dv, dv_s, ntf_s)
    amv = lf_synthesis(dv, dv_s) if synthesis else None
    return dv, amv


def app_image_mode_lf_factor(mv_img, dv_s, ntf_s, dtype=np.float16, synthesis=True):
    mv = setup_mv(mv_img, dv_s) * dv_s.rank
    # blks = generate_blks(dv_s.frame_shape, (64, 64))
    # print(blks)
    dv: np.ndarray = setup_dv(dv_s, dtype)
    amv: np.ndarray = None

    mv = mv.astype(dtype)
    dv = dv.astype(dtype)
    dv = lf_fact(mv, dv, dv_s, ntf_s)
    if synthesis:
        amv = lf_synthesis(dv, dv_s)
        # amv = amv.astype(float)
    # dv = dv.astype(float)
    return dv, amv


HCI = [
    "antinous",
    "backgammon",
    "bedroom",
    "bicycle",
    "boardgames",
    "boxes",
    "cotton",
    "dino",
    "dishes",
    "dots",
    "greek",
    "herbs",
    "kitchen",
    "medieval2",
    "museum",
    "origami",
    "pens",
    "pillows",
    "platonic",
    "pyramids",
    "rosemary",
    "sideboard",
    "stripes",
    "table",
    "tomb",
    "tower",
    "town",
    "vinyl",
]

HCI_interest = [
    "cotton",
    "pens",
    "rosemary",
]


# %%
if __name__ == "__main__":
    D = "path-to-base-folder"
    layers = [0, 1]
    rank = 4
    ntf_s = NTFStruct((1, 100))
    # name = HCI[0]
    for name in HCI_interest[0:1]:
        folder = Path(f"{D}/Dataset/") / "IMG" / "HCI" / name
        output_folder = Path(f".") / ("img_" + name)
        print(folder)
        mv_f = MvField(file=str(folder))
        amvc = np.zeros((9, 9, 512, 512, 3))
        dvc = np.zeros((rank, 2, 528, 528, 3))
        dv_s = DvStruct(layers, rank, layers[-1], mv_f.shape[:4])
        mv_f.data *= dv_s.rank
        for ch in range(3):
            mvd = mv_f.data[..., ch]
            if ch == 0:
                dv, amv = image_mode_lf_factor(
                    mvd, dv_s, ntf_s, dtype=np.float64, synthesis=False
                )
            else:
                dv, amv = image_mode_lf_factor_given(
                    mvd, dv, dv_s, ntf_s, dtype=np.float64, synthesis=False
                )
            # print(psnr(mvd, amv))
            # export
            dv = dv.astype(float)
            cv.imwrite(f"dv_{ch}.png", dv[0, 0] * 255)
            # amv = amv.astype(float)
            # amvc[..., ch] = amv
            dvc[..., ch] = dv
        output_folder.mkdir(parents=True, exist_ok=True)
        # cv.imwrite(str(output_folder / "amv.png"), mv_f.data[0, 0] * 255)
        for r in range(rank):
            for l in dv_s.layers:
                l_idx, l_pos = l
                cv.imwrite(
                    str(output_folder / f"dv_{l_pos}_{r:06}.png"), dvc[r, l_idx] * 255
                )
