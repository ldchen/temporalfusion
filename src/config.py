import numpy as np
import json

INFO_flag = False
dtype = np.float16

def INFO(message: str):
    if INFO_flag:
        print(f'INFO: %s' % message)
def DEBUG(message: str):
    print(f"DEBUG: {message}")

def read_config(filename):
    with open(filename) as fp:
        config = json.load(fp)
    if config["block"] == "None":
        config["block"] = None
    return config


if __name__ == "__main__":
    pass
